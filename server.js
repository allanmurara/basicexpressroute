const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const router = require('./routes');
const { appSecret } = require('./secret.js');

const server = express();
const PORT = 8000;

const sessionOptions = {
  secret: appSecret,
  resave: false,
  secure: 'auto',
  saveUninitialized : false,
  cookie: { maxAge: 86400, expires: 86400 },
}

server.use(bodyParser.json());
server.use(session(sessionOptions));

router(server);

server.listen(PORT, () => {
  console.log(`Server is runing over ${PORT}`);
});
