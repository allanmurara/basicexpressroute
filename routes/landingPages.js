const sendUserError = require('./routeConstants').sendUserError;
const sendStatusOk = require('./routeConstants').sendStatusOk;

const put = (req, res) => {
  return sendStatusOk(res, 'put requisition');
}

const get = (req, res) => {
  return sendStatusOk(res, 'get requisition');
}


const post = (req, res) => {
  return sendStatusOk(res, 'post requisition');
}

module.exports = {
  put,
  get,
  post
};
