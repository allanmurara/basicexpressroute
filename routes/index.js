//declare routes and wrap the server over them
// must create a single endpoint "landing-pages" that supports GET, POST, PUT;
// must create a endpoint called pages that receives a slug and return an landpage
const landingPages = require('./landingPages');
//const pages = require('./pages');

module.exports = (server) => {
  server.get('/get', landingPages.get);
  server.post('/post', landingPages.post);
  server.put('/put', landingPages.put);
};
