const STATUS_USER_ERROR = 422;
const STATUS_NOT_FOUND = 400;
const STATUS_OK = 200;

const sendUserError = (res, msg = 'Something goes wrong') => res.status(STATUS_USER_ERROR).json({ error:msg });

const sendStatusOk = (res, msg = { ok: true }) => res.status(STATUS_OK).json(msg);

module.exports = { 
  STATUS_USER_ERROR,
  STATUS_NOT_FOUND,
  STATUS_OK,
  sendUserError,
  sendStatusOk,
};
